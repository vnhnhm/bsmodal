class RenameDeleteToRemove < ActiveRecord::Migration[5.1]
  def change
  	rename_column :users, :delete, :remove
  end
end
