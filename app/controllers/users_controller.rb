class UsersController < ApplicationController
  def index
  	@users = User.all 
  end

  def edit
    @user = User.find(params[:id])
  end

  def readToggle
  end

  def writeToggle
  end

  def deleteToggle
  end

  private

  def user_params
    params.require(:user).permit(:password)
  end
end
