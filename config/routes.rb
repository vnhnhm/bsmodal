Rails.application.routes.draw do
  get 'users/index'
  get "users/edit/:id" => 'users#edit', :as => :edit
  get 'users/readToggle'
  get 'users/writeToggle'
  get 'users/deleteToggle'
  devise_for :users
  root to: "users#index" #holder for now
end
