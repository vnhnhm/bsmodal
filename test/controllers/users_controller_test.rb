require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get users_index_url
    assert_response :success
  end

  test "should get readToggle" do
    get users_readToggle_url
    assert_response :success
  end

  test "should get writeToggle" do
    get users_writeToggle_url
    assert_response :success
  end

  test "should get deleteToggle" do
    get users_deleteToggle_url
    assert_response :success
  end

end
