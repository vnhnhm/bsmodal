require 'rails_helper'

RSpec.describe UsersController, type: :controller do

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #readToggle" do
    it "returns http success" do
      get :readToggle
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #writeToggle" do
    it "returns http success" do
      get :writeToggle
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #deleteToggle" do
    it "returns http success" do
      get :deleteToggle
      expect(response).to have_http_status(:success)
    end
  end
  describe "Index" do
    it "returns all the of users " do
      first = create(:user)
      second = create(:user, email: "Joe@joemail.com")
      three = create(:user, email: "linda@joemail.com")
      get :index
      #assigns(:index).each{|i| puts i.email}
      expect(assigns(:users).length).to eq(3)
    end
  end
end
